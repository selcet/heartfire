(function () {
"use strict";

    var advertisingButtons = document.querySelectorAll('.userItem__buyAds'),
        modalWindow = document.querySelector('.modalBuyAds'),
        modalWindowOverlay = document.querySelector('.modal__overlay'),
        dropdown = modalWindow.querySelector('.dropdown'),
        dropdownTitle = dropdown.querySelector('.dropdown__title'),
        dropdownItems = modalWindow.querySelectorAll('.dropdown__item');

    console.log(dropdown);

    function showHideElement() {
        if (this.classList.contains('active')) {
            this.classList.remove('active');
        } else {
            this.classList.add('active');
        }
    }

    advertisingButtons.forEach(function (item, idx) {
        item.addEventListener("click", showHideElement.bind(modalWindow), false);
    });

    dropdownItems.forEach(function (item, idx) {
        item.addEventListener("click", showHideElement.bind(dropdown), false);
    });

    modalWindowOverlay.addEventListener("click", showHideElement.bind(modalWindow), false);
    dropdownTitle.addEventListener("click", showHideElement.bind(dropdown), false);
})();